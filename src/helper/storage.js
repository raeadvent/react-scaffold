const storage = {
  clear: () => {
    return new Promise((resolve, reject) => {
      try {
        localStorage.clear()
        resolve(null)
      } catch (err) {
        reject(err)
      }
    })
  },

  getItem: (key) => {
    return new Promise((resolve, reject) => {
      try {
        const value = localStorage.getItem(key)
        resolve(value)
      } catch (err) {
        reject(err)
      }
    })
  },

  removeItem: (key) => {
    return new Promise((resolve, reject) => {
      try {
        localStorage.removeItem(key)
        resolve(null)
      } catch (err) {
        reject(err)
      }
    })
  },

  setItem: (key, value) => {
    return new Promise((resolve, reject) => {
      try {
        localStorage.setItem(key, value)
        resolve(null)
      } catch (err) {
        reject(err)
      }
    })
  },
}

export default storage
