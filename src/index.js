import StoreProvider, { useStore } from './component/StoreProvider'
import persistence from './helper/persist'
import { ReachProtected } from './component/reach-protected'
import useStates from './hooks/useStates'
import { EaseIn, PageTransition } from './component/animations'

export {
  StoreProvider,
  EaseIn,
  PageTransition,
  useStore,
  persistence,
  ReachProtected,
  useStates,
}
