import React, { Fragment } from 'react'

import { Redirect } from '@reach/router'

export const ReachProtected = ({
  children,
  redirectUrl = '',
  condition = true,
  customFallback,
}) => {
  if (!condition) {
    if (customFallback) {
      return customFallback()
    }

    return <Redirect to={redirectUrl} />
  }

  return <Fragment>{children}</Fragment>
}
