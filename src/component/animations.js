import React from 'react'
import { motion } from 'framer-motion'
import { Location } from '@reach/router'

export function EaseIn({ children }) {
  const initialState = { y: '300px', opacity: 0 }

  const animations = { y: 0, opacity: 1 }

  const transitionSettings = { ease: 'easeIn', duration: 0.5 }
  const list = {
    visible: { opacity: 1 },
    hidden: { opacity: 0 },
  }

  return (
    <motion.div
      initial={initialState}
      animate={animations}
      transition={transitionSettings}
      variants={list}
    >
      {children}
    </motion.div>
  )
}

export function PageTransition({ children, route }) {
  return (
    <Location>
      {(props) => {
        return (
          <motion.div
            key={props.location.pathname}
            initial="pageInitial"
            animate="pageAnimate"
            variants={{
              pageInitial: {
                opacity: 0,
              },
              pageAnimate: {
                opacity: 1,
              },
            }}
          >
            {children}
          </motion.div>
        )
      }}
    </Location>
  )
}
