import React from 'react'

/**
 *
 * @param object initialValues of the states, separated by key
 * @returns
 */

export default function useStates(initialValues) {
  const [states, setStates] = React.useState(initialValues)
  // set value to state
  const setState = (key, value) => {
    const newState = {
      ...states,
      [key]: value,
    }

    setStates(newState)
  }

  return [states, setState]
}
