# react-scaffold

> A Collection of simple react components and custom hooks to speed up development by removing repetitive task when setting up react app (redux, router, hooks, etc)

[![NPM](https://img.shields.io/npm/v/react-scaffold.svg)](https://www.npmjs.com/package/react-scaffold) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-scaffold
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-scaffold'
import 'react-scaffold/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Raeneldis A. Sadra](https://github.com/Raeneldis A. Sadra)
