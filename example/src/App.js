import React from 'react'

import { ExampleComponent } from 'react-scaffold'
import 'react-scaffold/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
